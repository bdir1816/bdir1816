package salariati.repository.implementations;

import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.repository.implementations.EmployeeImpl;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeImplTest {

    @Test
    public void changeFunction() {

        ArrayList<Employee> list = new ArrayList<Employee>();

        Employee e1 = new Employee("Lastname","2960702888888",DidacticFunction.ASISTENT,"2000");
        Employee e2 = new Employee("Lastname1","2960702888888",DidacticFunction.ASISTENT,"2000");
        Employee e3 = new Employee("Lastname2","2960702888888",DidacticFunction.LECTURER,"2000");
        list.add(e1);
        list.add(e2);
        list.add(e3);

        Employee old = e1;

        Employee empl = new Employee("Lastname2","2960702888888",DidacticFunction.LECTURER,"2000");

        assertTrue(functie(list,empl));
       // assertTrue(changeFunction1(list,old,empl));
        empl.setLastName("Notregistered");
        assertFalse(functie(list,empl));

    }

    public boolean functie(ArrayList<Employee> list, Employee empl){

        int ok = 0;
        int nr=0;
        while (nr<list.size()) {
            Employee employee = new Employee();
            try {
                employee = list.get(nr);
                if(employee.getLastName().equals(empl.getLastName())){
                    ok = 1;
                    //list.remove(nr);
                    employee.setFunction(empl.getFunction());
                }
                //employeeList.add(employee);
                nr = nr +1;

            }
            catch (Exception e){
                return false;
            }
        }
        if(ok == 1) {
            return true;
        }
        else {
            return false;
        }
    }
/*
    @Test
    public boolean changeFunction1(ArrayList<Employee> list,Employee old, Employee empl) {
        int ok = 0;
        int nr=0;
        while (nr<list.size()) {
            Employee employee = new Employee();
            try {
                employee = list.get(nr);
                //System.out.println(old.getLastName() + " if " + empl.getLastName());
                if(employee.getLastName().equals(old.getLastName())){
                    ok = 1;
                    //list.remove(nr);
                    employee.setFunction(empl.getFunction());
                }
                //employeeList.add(employee);
                nr = nr +1;

            }
            catch (Exception e){
                System.out.println("Erroare");
            }
        }
        if( ok == 1) {
            return true;
        }
        else {
            return false;

        }
    }
    */
    @Test
    public void testCase1(){

        Employee old = new Employee("Marioara","2960702124931",DidacticFunction.TEACHER,"2700");
        Employee newEmpl = new Employee("Marioara","2960702124931",DidacticFunction.LECTURER,"2700");


    }
}