package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {
	
	//private final String employeeDBFile = "salariati/repository/implementation/employeeDB/employees.txt";
	private final String employeeDBFile = "E:\\Sem 2\\VVSS\\02-ProiectSalariati\\02-ProiectSalariati\\ProiectSalariati\\src\\main\\java\\salariati\\repository\\implementations\\employeeDB\\employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.newLine();
				bw.write(employee.toString());
				//bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		ArrayList<Employee> employeeList = new ArrayList<Employee>();

		BufferedReader br = null;
		int ok = 0;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					if(employee.getLastName().equals(oldEmployee.getLastName())){
						ok = 1;
							//list.remove(nr);
						employee.setFunction(newEmployee.getFunction());
					}
					employeeList.add(employee);
					counter = counter+1;
				} catch(EmployeeException ex) {
					System.err.println("Error while readhhing: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}

		try {
			BufferedWriter bw = null;
			for (int i = 0; i < employeeList.size(); i++) {
				if(i==0) {
					bw = new BufferedWriter(new FileWriter(employeeDBFile));
				}
				else {
					bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
					bw.newLine();
				}
				//bw = new PrintWriter(employeeDBFile);

				//bw.write(employeeList.get(i).toString());
				bw.write(employeeList.get(i).toString());

				//bw.newLine();
				bw.close();

			}

		}
 		catch (IOException e) {
				e.printStackTrace();
			}

		if (changeFunction(employeeList,oldEmployee,newEmployee)){
			System.out.println("Functia angajatului a fost modificata cu succes!");
		}
		else {
			System.out.println("Angajatul cu acest nume nu exista.");
		}
		
	}

	public boolean changeFunction(ArrayList<Employee> list, Employee old, Employee empl) {
		int ok = 0;
        int nr=0;
        while (nr<list.size()) {
            Employee employee = new Employee();
            try {
                employee = list.get(nr);
                //System.out.println(old.getLastName() + " if " + empl.getLastName());
                if(employee.getLastName().equals(old.getLastName())){
                    ok = 1;
                    //list.remove(nr);
                    employee.setFunction(empl.getFunction());
                }
                //employeeList.add(employee);
                nr = nr +1;

            }
            catch (Exception e){
                return false;
            }
        }
        if( ok == 1) {
			return true;
		}
		else {
        	return false;

		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
					counter = counter+1;
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}

		for(int i=0;i<employeeList.size();i++){
			for(int j=i+1;j<employeeList.size();j++){
				if(Integer.parseInt(employeeList.get(i).getSalary()) < Integer.parseInt(employeeList.get(j).getSalary())){
					Employee first = employeeList.get(i);
					Employee second = employeeList.get(j);
					employeeList.set(i,second);
					employeeList.set(j,first);
				}
			}
		}

		for(int i=0;i<employeeList.size();i++){
			for(int j=i+1;j<employeeList.size();j++){
				if(Integer.parseInt(employeeList.get(i).getSalary()) == Integer.parseInt(employeeList.get(j).getSalary())){
					int an_first = Integer.parseInt(employeeList.get(i).getCnp().substring(1,3));
					int an_second = Integer.parseInt(employeeList.get(j).getCnp().substring(1,3));
					if(an_first < an_second){
						Employee first = employeeList.get(i);
						Employee second = employeeList.get(j);
						employeeList.set(i,second);
						employeeList.set(j,first);
					}

				}
			}
		}




		return employeeList;
	}

	/*public List<Employee> getEmployeeListFromList(List<Employee> list) {
		List<Employee> listaProv;
		listaProv = list;

		for (int i = 0; i < listaProv.size(); i++) {
			for (int j = i + 1; j < listaProv.size(); j++) {
				if (Integer.parseInt(listaProv.get(i).getSalary()) < Integer.parseInt(listaProv.get(j).getSalary())) {
					Employee first = listaProv.get(i);
					Employee second = listaProv.get(j);
					listaProv.set(i, second);
					listaProv.set(j, first);
				}
			}
		}

		for (int i = 0; i < listaProv.size(); i++) {
			for (int j = i + 1; j < listaProv.size(); j++) {
				if (Integer.parseInt(listaProv.get(i).getSalary()) == Integer.parseInt(listaProv.get(j).getSalary())) {
					int an_first1 = Integer.parseInt(listaProv.get(i).getCnp().substring(1, 3));
					int an_second = Integer.parseInt(listaProv.get(j).getCnp().substring(1, 3));
					if (an_first1 < an_second) {
						Employee first = listaProv.get(i);
						Employee second = listaProv.get(j);
						listaProv.set(i, second);
						listaProv.set(j, first);
					}

				}
			}
		}
		return listaProv;
	}

*/
	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		return employeeList;
	}

	public void getEmployeeRep(String name, DidacticFunction functie){
		List<Employee> employeeList = new ArrayList<Employee>();

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					if(employee.getLastName() == name){
						employee.setFunction(functie);
					}
					employeeList.add(employee);
					counter = counter+1;
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}


		BufferedWriter bw = null;
		for(int i=0;i<employeeList.size();i++) {
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.newLine();
				bw.write(employeeList.get(i).toString());
				//bw.newLine();
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
