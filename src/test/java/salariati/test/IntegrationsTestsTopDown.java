package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static salariati.enumeration.DidacticFunction.CONFERENTIAR;

public class IntegrationsTestsTopDown {
    private EmployeeMock employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        // controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    //testare unitara modul A
    public void testAddEmployeeAndRepositoryMock() {
        assertFalse(employeeRepository.getEmployeeList().isEmpty());
        assertEquals(7, employeeRepository.getEmployeeList().size());

        Employee newEmployee = new Employee("Elena", "1910509055057", DidacticFunction.ASISTENT, "1000");
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.addEmployee(newEmployee);
        assertEquals(8, employeeRepository.getEmployeeList().size());
        assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }


    @Test
    //testare de integrare modul B: P->A->B
    public void testIntegration1() {
        //A
        Employee newEmployee = new Employee("Vio", "1910509055057", DidacticFunction.ASISTENT, "3000");
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.addEmployee(newEmployee);
        assertEquals(8, employeeRepository.getEmployeeList().size());

        //B
        String newFunc="CONFERENTIAR";
        Employee oldd = employeeRepository.getEmployeeList().get(7);
        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
        employeeRepository.modifyEmployee(oldd,neww);
        assertEquals(CONFERENTIAR,employeeRepository.getEmployeeList().get(7).getFunction());

    }

    @Test
    //test de integrare modul C: P -> B -> A -> C
    public void testIntegration2() {
        //B
        String newFunc="CONFERENTIAR";
        Employee oldd = employeeRepository.getEmployeeList().get(6);
        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
        employeeRepository.modifyEmployee(oldd,neww);
        assertEquals(CONFERENTIAR,employeeRepository.getEmployeeList().get(6).getFunction());

        //A
        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        Employee newEmployee2 = new Employee("Validd", "1920509055057", DidacticFunction.ASISTENT, "4000");

        employeeRepository.addEmployee(newEmployee);
        employeeRepository.addEmployee(newEmployee2);
        assertEquals(9, employeeRepository.getEmployeeList().size());

        //C
        List<String> listaOrdonata = Arrays.asList("Validd", "ValidLastName", "Bica","Daniel", "George","Ioana","Ionut", "Pop", "Ppp");

        List<Employee> listaFinala;
        listaFinala = employeeRepository.getEmployeeListFromList(employeeRepository.getEmployeeList());

        for (int i = 0; i < listaFinala.size(); i++) {
            assertEquals(listaFinala.get(i).getLastName(), listaOrdonata.get(i));
        }
    }
}