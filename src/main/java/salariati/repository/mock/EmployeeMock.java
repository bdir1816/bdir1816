package salariati.repository.mock;

import java.util.ArrayList;
import java.util.List;

import salariati.enumeration.DidacticFunction;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeMock implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	
	public EmployeeMock() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();
		
		Employee Bica   = new Employee("Bica", "1994567890876", DidacticFunction.ASISTENT, "2500");
		Employee Daniel   = new Employee("Daniel", "1984567890876", DidacticFunction.LECTURER, "2500");
		Employee Ioana  = new Employee("Ioana", "1894567890876", DidacticFunction.LECTURER, "2500");
		Employee Pop = new Employee("Pop", "1604567890876", DidacticFunction.ASISTENT, "2500");
		Employee George  = new Employee("George", "1904567890876", DidacticFunction.TEACHER,  "2500");
		Employee Ionut   = new Employee("Ionut", "1884567890876", DidacticFunction.TEACHER,  "2500");
		Employee Ppp = new Employee("Ppp","1234567892345",DidacticFunction.ASISTENT,"2000");
		employeeList.add( Bica );
		employeeList.add( Daniel );
		employeeList.add( Ioana );
		employeeList.add( Pop );
		employeeList.add( George );
		employeeList.add( Ionut );
		employeeList.add(Ppp);
	}
	
	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		oldEmployee.setFunction(newEmployee.getFunction());
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public List<Employee> getEmployeeListFromList(List<Employee> list){
		List<Employee> listaProv;
		listaProv = list;

		for (int i = 0; i < listaProv.size(); i++) {
			for (int j = i + 1; j < listaProv.size(); j++) {
				if (Integer.parseInt(listaProv.get(i).getSalary()) < Integer.parseInt(listaProv.get(j).getSalary())) {
					Employee first = listaProv.get(i);
					Employee second = listaProv.get(j);
					listaProv.set(i, second);
					listaProv.set(j, first);
				}
			}
		}

		for (int i = 0; i < listaProv.size(); i++) {
			for (int j = i + 1; j < listaProv.size(); j++) {
				if (Integer.parseInt(listaProv.get(i).getSalary()) == Integer.parseInt(listaProv.get(j).getSalary())) {
					int an_first1 = Integer.parseInt(listaProv.get(i).getCnp().substring(1, 3));
					int an_second = Integer.parseInt(listaProv.get(j).getCnp().substring(1, 3));
					if (an_first1 < an_second) {
						Employee first = listaProv.get(i);
						Employee second = listaProv.get(j);
						listaProv.set(i, second);
						listaProv.set(j, first);
					}

				}
			}
		}
		return listaProv;
	}

	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		// TODO Auto-generated method stub
		return null;
	}

}
