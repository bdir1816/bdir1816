package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ModifyEmployeeTest {

    private EmployeeMock employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }


    @Test
    public void testModify() {
        ArrayList<Employee> list = new ArrayList<Employee>();
        assertFalse(controller.getEmployeesList().isEmpty());

        Employee old = new Employee("Marioara","2960702124931",DidacticFunction.TEACHER,"2700");
        Employee newEmpl = new Employee("Marioara","2960702124931",DidacticFunction.LECTURER,"2700");

        //employeeRepository.addEmployee(old);
        list.add(old);

        employeeRepository.modifyEmployee(old,newEmpl);
        //System.out.println(old.getFunction() + " " + newEmpl.getFunction());
        assertTrue(old.getFunction() == newEmpl.getFunction());

    }
    @Test
    public void testModify1(){
        ArrayList<Employee> list = new ArrayList<Employee>();
        assertFalse(controller.getEmployeesList().isEmpty());

        Employee old = new Employee("Ioana","2960702124931",DidacticFunction.TEACHER,"2700");
        Employee newEmpl = new Employee("Marioara","2960702124931",DidacticFunction.LECTURER,"2700");

        //employeeRepository.addEmployee(old);
        list.add(old);

        employeeRepository.modifyEmployee(old,newEmpl);
        //System.out.println(old.getFunction() + " " + newEmpl.getFunction());
        assertTrue(old.getFunction() == newEmpl.getFunction());
    }

    @Test
    public void testModify2(){
        ArrayList<Employee> list = new ArrayList<Employee>();
        assertFalse(controller.getEmployeesList().isEmpty());

        Employee old = new Employee("Mihai","2960702124931",DidacticFunction.TEACHER,"2700");
        try {
            Employee newEmpl = new Employee("Marioara", "2960702124931", DidacticFunction.valueOf("Eroare"), "2700");
            employeeRepository.modifyEmployee(old,newEmpl);
            list.add(old);
            //System.out.println(old.getFunction() + " " + newEmpl.getFunction());
            assertFalse(old.getFunction() == newEmpl.getFunction());
        }
        catch (Exception e){
            //System.out.println(e.toString());
            assert( e.toString().equals("java.lang.IllegalArgumentException: No enum constant salariati.enumeration.DidacticFunction.Eroare"));
        }
        //employeeRepository.addEmployee(old);
    }

    @Test
    public void testModify3(){
        ArrayList<Employee> list = new ArrayList<Employee>();
        assertFalse(controller.getEmployeesList().isEmpty());

        Employee old = new Employee("Mihai","2960702124931",DidacticFunction.TEACHER,"2700");
        try {
            Employee newEmpl = new Employee("Marioara", "29607024931", DidacticFunction.LECTURER, "300");
            employeeRepository.modifyEmployee(old,newEmpl);
            list.add(old);
            //System.out.println(list.size());
            assert(list.size() == 1);
            //System.out.println(old.getFunction() + " " + newEmpl.getFunction());
            assertTrue(old.getFunction() == newEmpl.getFunction());
        }
        catch (Exception e){
            //System.out.println(e.toString());
            assert( e.toString().equals("java.lang.IllegalArgumentException: No enum constant salariati.enumeration.DidacticFunction.Eroare"));
        }
        //employeeRepository.addEmployee(old);
    }

}