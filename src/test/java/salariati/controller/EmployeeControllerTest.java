package salariati.controller;

import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class EmployeeControllerTest {
    private EmployeeImpl employeeRepository;
    private final String employeeDBFile = "E:\\Sem 2\\VVSS\\02-ProiectSalariati\\02-ProiectSalariati\\ProiectSalariati\\src\\main\\java\\salariati\\repository\\implementations\\employeeDB\\employees.txt";

    @Test
    public void modifyEmployee() {
        employeeRepository = new EmployeeImpl();

        ArrayList<Employee> list = new ArrayList<Employee>();

        list.add(new Employee("Marioara","2960702124931",DidacticFunction.TEACHER,"2700"));
        list.add(new Employee("Iulia","2960702124931",DidacticFunction.TEACHER,"2700"));
        employeeRepository.addEmployee(new Employee("Andrei","2960702124931",DidacticFunction.TEACHER,"2700"));
        Employee old = new Employee("Marioara","2960702124931",DidacticFunction.TEACHER,"2700");
        Employee newEmpl = new Employee("Marioara","2960702124931",DidacticFunction.LECTURER,"2700");

        boolean ok = employeeRepository.changeFunction(list,old,newEmpl);
        assertTrue(ok);

        old.setLastName("Ionut");
        ok = employeeRepository.changeFunction(list,old,newEmpl);
        assertFalse(ok);



    }
}