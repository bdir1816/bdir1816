package salariati.main;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.repository.implementations.EmployeeImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

//functionalitati
//i.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//ii.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//iii.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
public class StartApp {
	
	public static void main(String[] args) {
		
		//EmployeeRepositoryInterface employeesRepository = new EmployeeMock();
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
		EmployeeController employeeController = new EmployeeController(employeesRepository);
		
		//for(Employee _employee : employeeController.getEmployeesList())
		//	System.out.println(_employee.toString());
		//System.out.println("-----------------------------------------");
		
		//Employee employee = new Employee("LastName", "1234567894321", DidacticFunction.ASISTENT, "2500");
		//employeeController.addEmployee(employee);
		
		for(Employee _employee : employeeController.getEmployeesList())
			System.out.println(_employee.toString());
		
		//EmployeeValidator validator = new EmployeeValidator();
		//System.out.println( validator.isValid(new Employee("LastName", "1234567894322", DidacticFunction.TEACHER, "3400")) );
		try {
			BufferedReader in = null;
			in = new BufferedReader(new InputStreamReader(System.in));
			int pick = -1;
			while (pick != 0) {
				if(pick != -1) {
					pick = Integer.parseInt(in.readLine());
				}
				menu();


			}
		}
		catch(IOException e){

		}
	}

	public static void menu(){
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
		EmployeeController employeeController = new EmployeeController(employeesRepository);

		System.out.println("1. Adauga un angajat");
		System.out.println("2. Modifica functia unui angajat");
		System.out.println("3. Afisare angajati in ordine");
		System.out.println("0. Iesire");
		System.out.println("Selectati optiunea:");

		BufferedReader in = null;
		in = new BufferedReader(new InputStreamReader(System.in));
		int option = -1;
		try{
			option = Integer.parseInt(in.readLine());
			switch(option){
				case 1:
					Employee newEmp = addNewEmployee();
					employeeController.addEmployee(newEmp);
					System.out.println("Angajatul a fost adaugat cu succes!");
					break;
				case 2:
					modifyEmployee();
					break;
				case 3:
					afisareOrdine();
					break;
				case 0:
					break;
			}
		}
		catch (Exception e){
			System.out.println(e.getStackTrace());
		}
	}
	public static Employee addNewEmployee(){
		String name,cnp,salary;
		DidacticFunction title;

		BufferedReader in = null;
		in = new BufferedReader(new InputStreamReader(System.in));

		try {
			EmployeeValidator validator = new EmployeeValidator();
			System.out.println("Nume:");
			name = in.readLine();
			System.out.println("Cnp:");
			cnp = in.readLine();
			System.out.println("Functia:");
			title = DidacticFunction.valueOf(in.readLine());
			System.out.println("Salariu:");
			salary = in.readLine();

			Employee newE = new Employee(name,cnp,title,salary);

			if (validator.isValid(newE) == true) {
				return newE;
			}
			else {
				System.out.println("Datele angajatului nu respecta cerintele");
			}

		}
		catch (IOException e){


		}
		Employee p = new Employee();
		return p;
	}
	public static void modifyEmployee(){
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
		EmployeeController employeeController = new EmployeeController(employeesRepository);


		BufferedReader in = null;
		in = new BufferedReader(new InputStreamReader(System.in));
		String name,newFunc;
		try {
			System.out.println("Numele angajatului:");
			name = in.readLine();
			Employee oldd = new Employee(name,"",DidacticFunction.ASISTENT,"");
			System.out.println("Noua functie a angajatului:");
			newFunc = in.readLine();
			if(newFunc.equals("TEACHER") || newFunc.equals("LECTURER") || newFunc.equals("CONFERENTIAR") || newFunc.equals("ASISTENT")){
				Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
				employeeController.modifyEmployee(oldd,neww);


				for(Employee _employee : employeeController.getEmployeesList())
					System.out.println(_employee.toString());

			}
			else {
				System.out.println("Aceasta activitate didactica nu poate fi atribuita.");
			}



		}
		catch(IOException e){}

	}
	public static void afisareOrdine(){
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
		EmployeeController employeeController = new EmployeeController(employeesRepository);

		List<Employee> listaFinala;
		listaFinala = employeeController.getEmployeesList();

		for(Employee _employee : listaFinala)
			System.out.println(_employee.toString());
	}

}
