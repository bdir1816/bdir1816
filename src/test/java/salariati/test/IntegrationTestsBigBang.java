package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static salariati.enumeration.DidacticFunction.CONFERENTIAR;

public class IntegrationTestsBigBang {
    private EmployeeMock employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        // controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void testAddEmployeeAndRepositoryMock() {
        assertFalse(employeeRepository.getEmployeeList().isEmpty());
        assertEquals(7, employeeRepository.getEmployeeList().size());

        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.addEmployee(newEmployee);
        assertEquals(8, employeeRepository.getEmployeeList().size());
        assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }


    @Test
    public void test1ModifyEmployee() {
        String newFunc="CONFERENTIAR";

        Employee oldd = employeeRepository.getEmployeeList().get(0);

        Employee neww = new Employee("","",DidacticFunction.CONFERENTIAR,"");

        employeeRepository.modifyEmployee(oldd,neww);
        System.out.println(oldd.getFunction());

        assertEquals(DidacticFunction.CONFERENTIAR,employeeRepository.getEmployeeList().get(0).getFunction());

    }

    @Test
    public void testSortEmployee() {
        List<String> listaOrdonata = Arrays.asList("Bica","Daniel", "George","Ioana","Ionut", "Pop", "Ppp");

        List<Employee> listaFinala;
        listaFinala = employeeRepository.getEmployeeListFromList(employeeRepository.getEmployeeList());


        for (int i = 0; i < listaFinala.size(); i++) {
            System.out.println(listaFinala.get(i).getLastName());
            assertEquals(listaFinala.get(i).getLastName(), listaOrdonata.get(i));
        }
    }

    @Test
    public void testCombinareModule() {
        //P->A->B->C
        //A: test adaugare
        Employee newEmployee = new Employee("Scrum", "1910509055057", DidacticFunction.ASISTENT, "3000");
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.addEmployee(newEmployee);
        assertEquals(8, employeeRepository.getEmployeeList().size());

        //B:test modificare
        String name="Scrum",newFunc="CONFERENTIAR";
        Employee oldd = employeeRepository.getEmployeeList().get(7);
        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
        employeeRepository.modifyEmployee(oldd,neww);
        assertEquals(CONFERENTIAR,employeeRepository.getEmployeeList().get(7).getFunction());

        //C:test sortare
        List<String> listaOrdonata = Arrays.asList("Scrum","Bica","Daniel", "George","Ioana","Ionut", "Pop", "Ppp");

        List<Employee> listaFinala1;
        listaFinala1 = employeeRepository.getEmployeeListFromList(employeeRepository.getEmployeeList());

        for (int i = 0; i < listaFinala1.size(); i++) {
            assertEquals(listaFinala1.get(i).getLastName(), listaOrdonata.get(i));
            //System.out.println(listaFinala.get(i).toString());
        }

    }

}