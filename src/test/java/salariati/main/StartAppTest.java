package salariati.main;

import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.*;

public class StartAppTest {

    @org.junit.Test
    public void addNewEmployee() {

        String name,cnp,salary;
        DidacticFunction title;

        EmployeeValidator validator = new EmployeeValidator();
        name = "Bica";
        cnp = "2960909888888";
        title = DidacticFunction.LECTURER;
        salary = "2500";

        Employee newE = new Employee(name,cnp,title,salary);

        assertTrue(validator.isValid(newE));

        newE.setLastName("bica");
        assertTrue(validator.isValid(newE));

        newE.setFunction(DidacticFunction.ASISTENT);
        assertTrue(validator.isValid(newE));

        newE.setSalary("9000");
        assertTrue(validator.isValid(newE));

        newE.setCnp("2908728372877");
        assertTrue(validator.isValid(newE));

        newE.setLastName("Bica Daniela");
        assertFalse(validator.isValid(newE));
        newE.setLastName("Bica");

        newE.setCnp("2960808888");
        assertFalse(validator.isValid(newE));
        newE.setCnp("2960909888888");

        newE.setLastName("Bica8");
        assertFalse(validator.isValid(newE));
        newE.setLastName("Bica");

        newE.setSalary("salary");
        assertFalse(validator.isValid(newE));
        newE.setSalary("salary");

        newE.setCnp("cnpcnpcnpcnpc");
        assertFalse(validator.isValid(newE));

        System.out.println("Teste ok");
    }
}