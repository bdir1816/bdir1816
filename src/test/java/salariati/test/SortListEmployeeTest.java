package salariati.test;


//import salariati.employeeRepository.EmployeeController;

import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static salariati.enumeration.DidacticFunction.ASISTENT;
import static salariati.enumeration.DidacticFunction.LECTURER;


//pentru lab 4
public class SortListEmployeeTest {
    private EmployeeMock employeeRepository;
    // private EmployeeController employeeRepository;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        //employeeRepository         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void testSortRepositoryMock1() {
        assertFalse(employeeRepository.getEmployeeList().isEmpty());
        assertEquals(7, employeeRepository.getEmployeeList().size());

        List<String> listaOrdonata= Arrays.asList("Bica","Daniel", "George","Ioana","Ionut", "Pop", "Ppp");

        List<Employee> listaFinala;
        listaFinala = employeeRepository.getEmployeeListFromList(employeeRepository.getEmployeeList());

        for (int i = 0; i < listaFinala.size(); i++) {
            assertEquals(listaFinala.get(i).getLastName(),listaOrdonata.get(i));
        }
    }

    @Test
    public void testSort2() {
        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        Employee newEmployee2 = new Employee("Valid", "1920509055057", DidacticFunction.ASISTENT, "4000");

        employeeRepository.addEmployee(newEmployee);
        employeeRepository.addEmployee(newEmployee2);


        List<String> listaOrdonata= Arrays.asList("Valid","ValidLastName","Bica","Daniel", "George","Ioana","Ionut", "Pop", "Ppp");

        List<Employee> listaFinala;
        listaFinala=employeeRepository.getEmployeeListFromList(employeeRepository.getEmployeeList());

        for (int i = 0; i < listaFinala.size(); i++) {
            assertEquals(listaFinala.get(i).getLastName(),listaOrdonata.get(i));
            //System.out.println(listaFinala.get(i).toString());
        }
    }


    @Test
    public void testSort3()  {
        Employee employee = new Employee("11111111 ","1234567890123", LECTURER, "2000");
        Employee e1 = new Employee("Catalina ","abcd", LECTURER, "-200");
        Employee e2 = new Employee("333 ","1234567890123", ASISTENT, "800");
        employeeRepository.addEmployee(employee);
        employeeRepository.addEmployee(e1);
        employeeRepository.addEmployee(e2);

        assertFalse(employeeValidator.isValid(employee));
        assertFalse(employeeValidator.isValid(e1));
        assertFalse(employeeValidator.isValid(e2));
        assertEquals(7, employeeRepository.getEmployeeList().size());


        List<String> listaOrdonata= Arrays.asList("Bica","Daniel", "George","Ioana","Ionut", "Pop", "Ppp");

        List<Employee> listaFinala;
        listaFinala=employeeRepository.getEmployeeListFromList(employeeRepository.getEmployeeList());

        for (int i = 0; i < listaFinala.size(); i++) {
            assertEquals(listaFinala.get(i).getLastName(),listaOrdonata.get(i));
        }

    }
}